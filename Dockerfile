#docker build -t registry.gitlab.com/iottacle/beacon-emitter:1.0.0 .
#docker run -ti -d -v /dev/hci0:/dev/hci0 --net=host -v /etc/mmma:/etc/mmma -v /opt/common:/opt/common --privileged --restart=unless-stopped registry.gitlab.com/iottacle/beacon-emitter:1.0.0

FROM balenalib/raspberrypi3-debian-python:3.7

RUN apt-get update
RUN apt-get install -y  libusb-dev \
                        libdbus-1-dev libglib2.0-dev libudev-dev libical-dev libreadline-dev \
                        bc bluez bluez-hcidump \
                        --fix-missing

WORKDIR /opt/beacon

RUN mkdir -p /opt/beacon

ADD requirements.txt run_beacon.sh Emitter.py /opt/beacon/

RUN chmod +x run_beacon.sh

RUN pip install -r requirements.txt

ENTRYPOINT ["/bin/bash", "run_beacon.sh"]
