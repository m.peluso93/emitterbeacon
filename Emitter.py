#!/usr/bin/python3
# coding=utf-8

import os
import threading
import datetime as dt
from threading import Thread
import uuid
import logging
import subprocess as sub
import signal


class Emitter(object):
    """
    pancakes
    """

    def __init__(self):
        """
        pancakes
        """
        self.devices = []
        self.emitDevice = self.get_device()
        self.stopSignal = False
        self.thread = None
        self.running = False
        self.__name__ = "Device Beacon Sniffer/Emitter"
        self.__version__ = "1.0.0"

        pass

    def find_device(self):
        """
          Read from the output of hcitool dev which interfaces are
          available
        :return:
        """
        os.system("hcitool dev | grep -E 'hci*' > interfaces.txt")

        with open('interfaces.txt', 'r') as f:
            for line in f:
                self.devices.append(line.strip().split()[0].replace(':', ''))
        pass

    def get_device(self):
        """
          pancakes
        :return:
        """
        self.find_device()
        try:
            if len(self.devices) == 0:
                return os.getenv('SCAN_DEVICE', "hci0")
            else:
                return self.devices[0]
        except Exception as e:
            print("Exception - get Device: ", e)
            return -1

    def enableBeaconMode(self):
        """
            Function which activate the Beacon Mode of a chosen bluethooth device
        """
        os.system("hciconfig %s down" % self.emitDevice)
        os.system("hciconfig %s up" % self.emitDevice)
        os.system("hciconfig %s leadv 3" % self.emitDevice)
        os.system("hciconfig %s noscan" % self.emitDevice)

        pass

    def start(self):
        """
           Function which start the process of sniffing
        """
        self.stopSignal = False
        self.thread = Thread(target=self.__run__)
        self.thread.start()

        pass

    def stop(self):
        """
                    Function which stop the process of sniffing
        """
        with threading.Lock() as lock:
            self.stopSignal = True
            self.thread.join()

            self.stopCmd()
            self.killprocess(process=self.pc)
            self.killprocess(process=self.puuid)
            self.killprocess(process=self.stopcmd)
        pass

    def defineMode(self):
        """

        :return:
        """
        modes = ["beacon", "superbeacon"]
        # Default mode beacon
        self.mode = os.getenv('MODE_BEACON', "beacon")

        if self.mode not in modes:
            print("Wrong mode inserted, possible mode are: ", str(modes))
            self.running = False
            self.stop()
            return

    def check_mode(self):
        tmp_mode = os.getenv('MODE_BEACON')
        if tmp_mode == self.mode:
            pass
        else:
            self.running = False
            self.stop()
            self.__run__()

    def generate_uuid(self):
        """
        pancakes
        :return:
        """
        return uuid.uuid4().hex

    def __run__(self):
        """


        :return:
        """
        k = 1
        self.defineMode()
        print("Mode, ", str(self.mode))
        if self.mode == 'superbeacon':
            pass
        elif self.mode == 'beacon':
            self.uuid = os.getenv('UUID', "aa aa aa aa bb bb cc cc dd dd ee ee ee ee ee ee")
            logging.info("Starting Emitter on: " + str(self.emitDevice))
            cmd = self.emitCmd()

            self.initUuid()
            print('started with UUID: ', self.uuid)
            logging.info("Enamble Beacon Mode, cmd: " + str(cmd))
            self.enableBeaconMode()

            start_time   = dt.datetime.now()
            minute_count = 1
            print('Start Emitting at: ', str(start_time))
            self.start_emit(cmd)

            while self.mode == os.getenv("MODE_BEACON"):
                delta_minutes = dt.datetime.now()
                if int(delta_minutes.minute - start_time.minute) > minute_count:
                    minute_count  = minute_count + int(delta_minutes.minute - start_time.minute)
                    print('Still emitting after {} minutes'.format(minute_count))

                with threading.Lock() as lock:
                    if self.stopSignal:
                        logging.debug("Received stop-signal")
                        break

            logging.info("Changed Beacon Mode, cmd: ")
            self.stopCmd()
            self.killprocess(process=self.pc)
            self.killprocess(process=self.puuid)
            self.killprocess(process=self.stopcmd)

        logging.info('Changing mode')

    def emitCmd(self):
        """

        :return:
        """
        return "hcitool -i " + str(self.emitDevice) + " cmd 0x08 0x000a 01"

    def stopCmd(self):
        """

        :return:
        """
        print('Stop CMD')
        self.stopcmd = sub.run("hcitool -i " + str(self.emitDevice) + " cmd 0x08 0x000a 00", shell=True)

    def killprocess(self, process):
        """

        :param process:
        :return:
        """
        os.killpg(os.getpgid(process.pid), signal.SIGTERM)

    def initUuid(self):
        print('init UUID')
        cmd = "hcitool -i " + str(self.emitDevice) + \
              " cmd 0x08 0x0008 1e 02 01 1a 1a ff 4c 00 02 15 " + \
              str(self.uuid) + \
              "01 00 01 00 c5"
        print('cmd uuid: ', cmd)
        self.puuid = sub.run(cmd, shell=True)

    def start_emit(self, cmd):
        """

        :return:
        """
        print('cmd: ', cmd)
        self.pc = sub.run(cmd, shell=True)
        self.initUuid()




if  __name__ == "__main__":
    emitter = Emitter()
    emitter.start()

